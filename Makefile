PKGNAME=glibc-grantpt-fix
SPECFILE=${PKGNAME}.spec
TARGET=libc_grantpt_fix.so
SRC=grantpt.c

PKGVERSION=$(shell grep -s '^Version:' $(SPECFILE) | sed -e 's/Version: *//')

libc_grantpt_fix.so:
	gcc -shared -fPIC -o ${TARGET} grantpt.c

clean:
	rm -f ${TARGET}

sources:
	rm -rf dist
	mkdir -p dist/${PKGNAME}-${PKGVERSION}
	cp -pr ${SRC} Makefile LICENSE dist/${PKGNAME}-${PKGVERSION}/.
	find dist -type d -name .svn | xargs -i rm -rf {}
	find dist -type d -name .git | xargs -i rm -rf {}
	cd dist ; tar cfz ../${PKGNAME}-${PKGVERSION}.tar.gz ${PKGNAME}-${PKGVERSION}
	rm -rf dist

srpm: sources
	rpmbuild -bs --define "_sourcedir ${PWD}" ${SPECFILE}

rpm: sources
	rpmbuild -ba --define "_sourcedir ${PWD}" ${SPECFILE}

install: libc_grantpt_fix.so
	mkdir -p $(DESTDIR)/usr/lib64
	install -m 644 ${TARGET} $(DESTDIR)/usr/lib64/
