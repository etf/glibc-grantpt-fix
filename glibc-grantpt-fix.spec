%define name glibc-grantpt-fix
%define version 1.0.0
Name:    %{name}
Version: 1.0.0
Release: 1%{?dist}
Summary: Hotfix for glibc flaw when acquiring pseduterminal in user namespace with setuid off (relevant for singularity/apptainer)

License: ASL 2.0
URL:     https://gitlab.cern.ch/etf/glibc-grantpt-fix
Source0: %{name}-%{version}.tar.gz
Vendor:  Marian Babik <<marian.babik@cern.ch>>
Packager: Marian Babik <marian.babik@cern.ch>


BuildRequires:  gcc
BuildRequires:  make

%description
Hotfix for glibc flaw when acquiring pseduterminal in user namespace with setuid off. This issue is relevant for any singularity/apptainer
deployments where pty allocation is needed (https://github.com/apptainer/apptainer/issues/297).

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
%make_install 

%files
%license LICENSE
%{_libdir}/libc_grantpt*

%changelog
* Wed Mar 16 2022 Marian Babik <marian.babik@cern.ch> 1.0.0
- First release

